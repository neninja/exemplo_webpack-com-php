<?php
define('ENV', 'dev');
// define('ENV', 'prod');
$pathPublic = ENV === 'dev'
    ? 'http://localhost:4444/public'
    : 'http://localhost:1234/public';
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <title></title>
</head>
<body>
ENV: <?=ENV?>

    <!--<script src="public/bundle.js"></script>-->
<script src="<?=$pathPublic?>/bundle.js"></script>

</body>

</html>

