const path = require('path')

module.exports = {
    entry: path.resolve(__dirname, 'src', 'index.js'),
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: 'http://localhost:1234/public/', // https://blog.chrisworfolk.com/how-to-run-webpack-dev-server-with-php
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        port: 4444
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            }
        ]
    }
};
